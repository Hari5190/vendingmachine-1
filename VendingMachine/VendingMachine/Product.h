#pragma once
#include<string>
#include<iostream>
using namespace std;

class Product
{
private:
	string productname;
	int cost;
	int units_left;

public:
	Product(string name, int cost, int units);
	string GetName();
	int GetCost();
	int GetUnitsLeft();
	void DispatchUnitProduct();
	~Product();
};

