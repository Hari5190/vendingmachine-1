#pragma once
#include<vector>
#include "Product.h"
#include<set>

enum STATE {
	VALIDATION_FAILURE = -1,
	SUCCESS = 0,
	VALIDATION_SUCESS = 1,
	EXIT_FROM_APPLICATION = 2,
	ENTER_AS_ADMINISTRATOR = 3,
	EXIT_AS_ADMINISTRATOR = 4	
};
class Machine
{
private:
	vector<Product*> products;
	int money;
	const int MAX_STOCK = 10;
	const int MAX_PRODUCTS = 10;
	const set<int> DENOMINATIONS = { 5,10,20};
	const string PASSWORD = "test1234";
	int ValidateRequest(int id);
	int ValidateDenominations(string s,int cost);
public:
	Machine();
	void DisplayOptions();
	void FetchProduct(int id);
	int  ProcessRequest(int id);
	vector<Product*> GetProducts();
	void AddToMoney(int x);
	~Machine();

};