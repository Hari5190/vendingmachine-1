#pragma once
#include "Machine.h"
class AdminstratorMachine :
	public Machine
{
private:
	shared_ptr<Machine> machine;
public:
	AdminstratorMachine(shared_ptr<Machine> mach);
	~AdminstratorMachine();
	void DisplayAdminOptions();
	int ProcessAdminRequests(int inp);
	void AddProduct();
	void RemoveProduct();
	void RefillVendingMachineProduct(int id);
	void GetMachineStatus();
};