// VendingMachine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include "Machine.h"
#include "AdminstratorMachine.h"

using namespace std;

int main()
{
	cout << "Welcome to MathWorks Vending Machine...\n";
	int input;
	shared_ptr<Machine> vm(new Machine());
	while (1) {
		cout << "Please choose you option from the Below Menu.." << endl;
		vm->DisplayOptions();
		cin >> input;
		cout << "You have chosen option " << input << endl;
		int return_val = vm->ProcessRequest(input);
		if (return_val == EXIT_FROM_APPLICATION) {
			//User opted out of this Program
			cout << "Thank you for Using our Vending Machine..Have a Nice Day..GoodBye" << endl;
			break;
		}
		if (return_val == ENTER_AS_ADMINISTRATOR) {
			while (1) {
				unique_ptr<AdminstratorMachine> admin(new AdminstratorMachine(vm));
				admin->DisplayAdminOptions();
				int input;
				cin >> input;
				int return_val = admin->ProcessAdminRequests(input);
				if (return_val == EXIT_AS_ADMINISTRATOR) {
					break;
				}
			}
		}
	}
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

