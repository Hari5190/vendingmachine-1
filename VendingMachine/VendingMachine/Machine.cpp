#include "pch.h"
#include "Machine.h"
#include <iostream>
#include <string>
#include "Product.h"
using namespace std;

	Machine::Machine() {
		//Creating Default Products
		Product *coffee = new Product("Hot Coffee", 20, MAX_STOCK);
		products.push_back(coffee);
		Product *greentea = new Product("Green Tea", 10, MAX_STOCK);
		products.push_back(greentea);
		Product *capuchino = new Product("Capuchino", 25, MAX_STOCK);
		products.push_back(capuchino);
		Product *latino = new Product("Latino", 25, MAX_STOCK);
		products.push_back(latino);
		Product *espresso = new Product("Espresso", 30, MAX_STOCK);
		products.push_back(espresso);
		Product *lemontea = new Product("Lemon Tea", 15, MAX_STOCK);
		products.push_back(lemontea);
		Product *hotmilk = new Product("Hot Milk", 15, MAX_STOCK);
		products.push_back(hotmilk);
	}
	
	void Machine::DisplayOptions() {
		cout << "PLEASE NOTE THAT WE ACCEPT BELOW DENOMINATIONS ONLY ";
		set<int>::iterator it;
		for (it = DENOMINATIONS.begin(); it != DENOMINATIONS.end(); ++it)
			cout << " " << *it;
		cout << endl;

		int size = products.size();

		for (int i = 0; i < size; i++) {
			cout << i + 1 << ". " << products.at(i)->GetName() << "\t Cost " <<
				products.at(i)->GetCost() << " Rs \t" << products.at(i)->GetUnitsLeft() << " units Left" << endl;
		}
		//Displaying Extraa Options
		cout << size + 1 << ". Adminstrator Mode.." << endl;
		cout << size + 2 << " Exit from this Machine" << endl;
	}

	void Machine::FetchProduct(int id) {
		products.at(id)->DispatchUnitProduct();
	}

	vector<Product*> Machine::GetProducts() {
		return products;
	}

	int Machine::ValidateRequest(int id) {
		int size = products.size();
		if (id == size + 2) {
			return EXIT_FROM_APPLICATION;
		}
		if (id == size + 1) {
			cout << "Please Enter the Password to Login as Administrator" << endl;
			string password;
			cin >> password;
			if (PASSWORD.compare(password) == 0) {
				cout << "You are running as Administrator" << endl;
				return ENTER_AS_ADMINISTRATOR;
			}
			else {
				cout << "Invalid Password..Returning to Main Menu" << endl;
				return VALIDATION_FAILURE;
			}
			
		}
		id -= 1;
		if (id<0 || id > size) {
			cout << "You have entered an invalid Option ...Please enter a Valid option" << endl;
			return VALIDATION_FAILURE;
		}
		return VALIDATION_SUCESS;
	}
	int Machine::ValidateDenominations(string user_denominations , int cost) {
		vector<int> denoms;
		int x = 0;
		int sum = 0;
		for (int i = 0; i < user_denominations.length(); i++) {
			if (user_denominations.at(i) == ',') {

				denoms.push_back(stoi(user_denominations.substr(x, i - x)));
				x = i + 1;
			}
		}
		denoms.push_back(stoi(user_denominations.substr(x)));
		for (int i = 0; i < denoms.size(); i++) {
			if (DENOMINATIONS.count(denoms.at(i)) == 0) {
				cout << "Invalid Denomination " << denoms.at(i) << endl;
				return VALIDATION_FAILURE;
			}
			else {
				sum = sum + denoms.at(i);
			}
		}
		if (sum < cost) {
			cout << "You have entered " << cost - sum << " Rs less"<<endl;
			return VALIDATION_FAILURE;
		}
		else if (sum > cost) {
			cout << "You have entered " << sum - cost << " Rs more .."<<endl;
			return VALIDATION_FAILURE;
		}
		return VALIDATION_SUCESS;
	}

	int  Machine::ProcessRequest(int id) {
		int retval = ValidateRequest(id);
		if (retval == VALIDATION_FAILURE || retval == EXIT_FROM_APPLICATION || retval == ENTER_AS_ADMINISTRATOR) 
			 return retval;
		id -= 1;
		//No Validation Errors
		Product* product_temp = products.at(id);
		if (product_temp == NULL) {
			cout << "Unexpected issue in Fetching Products..We are out of service currently";
			return VALIDATION_FAILURE;
		}
		if (product_temp->GetUnitsLeft() == 0) {
			cout << "Sorry ..No stock left for  " << product_temp->GetName() << endl;
			cout << "Please chose another option";
			return VALIDATION_FAILURE;
		}
		cout << "Thank you for choosing " << product_temp->GetName() << ".. Please pay us " <<
			product_temp->GetCost() <<" Rs" <<endl;
		string user_denominations;
		cout << "Please enter your denominations (comma separted)" << endl;
		cin >> user_denominations;
		retval = ValidateDenominations(user_denominations, product_temp->GetCost());
		//Handle Logic for denominations
		if (retval == VALIDATION_SUCESS) {
			cout << "Pleae have your " << product_temp->GetName() << endl;
			product_temp->DispatchUnitProduct();
		}
		
		return SUCCESS;
		
	}
	void Machine::AddToMoney(int x) {
		money += x;
	}
	Machine::~Machine() {
	}