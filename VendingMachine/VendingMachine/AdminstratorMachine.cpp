#include "pch.h"
#include "AdminstratorMachine.h"


AdminstratorMachine::AdminstratorMachine(shared_ptr<Machine> mach) : machine(mach)
{
}


AdminstratorMachine::~AdminstratorMachine()
{
}

void AdminstratorMachine::AddProduct() {
	cout << "Adding a Product...yet to be implmented" << endl;
}
void AdminstratorMachine::RemoveProduct() {
	cout << "Removing a Product......yet to be implmented" << endl;
}

void AdminstratorMachine::RefillVendingMachineProduct(int id) {
	cout << "Refilling product......yet to be implmented " << id << endl;

}

int AdminstratorMachine::ProcessAdminRequests(int input) {
	switch (input) {
	case 1: { AddProduct(); return SUCCESS; break; }
	case 2: { RemoveProduct(); return SUCCESS; break; }
	case 3: { RefillVendingMachineProduct(5); return SUCCESS; break; }
	case 4: { GetMachineStatus(); return SUCCESS; break; }
	case 5: { return EXIT_AS_ADMINISTRATOR; break; }
	default: return 0;
	}
}
void AdminstratorMachine::DisplayAdminOptions() {
	cout << "\nAdministrator Options ..Please Enter your choice\n";
	cout << "1. Add a Product\n";
	cout << "2. Remove a Product\n";
	cout << "3. Refill a Product\n";
	cout << "4. Get Machine Status\n";
	cout << "5. Exit as Administrator" << endl;
}

void AdminstratorMachine::GetMachineStatus() {
	vector<Product*> products = machine->GetProducts();
	int size = products.size();

	for (int i = 0; i < size; i++) {
		cout << i + 1 << ". " << products.at(i)->GetName() << "\t Cost " <<
			products.at(i)->GetCost() << " Rs \t" << products.at(i)->GetUnitsLeft() << " units Left" << endl;
	}
}