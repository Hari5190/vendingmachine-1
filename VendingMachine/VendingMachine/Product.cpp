#include "pch.h"
#include "Product.h"
#include <iostream>
#include <string>
using namespace std;

	Product::Product(string name, int pcost, int units):
		productname(name),cost(pcost),units_left(units) {
	}
	string Product::GetName() {
		return productname;
	}
	int Product::GetCost() {
		return cost;
	}
	int Product::GetUnitsLeft() {
		return units_left;
	}
	void Product::DispatchUnitProduct() {
		units_left -= 1;
	}
	Product::~Product() {

	}